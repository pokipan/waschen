import nextcord, os, re, sqlite3, time, datetime, feedparser
from utils import *
from nextcord import Interaction
from nextcord.ext import commands, application_checks, tasks
from dotenv import load_dotenv

load_dotenv()

intents = nextcord.Intents.all()
client = nextcord.ext.commands.Bot(intents=intents)

conn = sqlite3.connect("waschen.sqlite3")
c = conn.cursor()

class Toolkit():
    def __init__(self, client, c, conn):
        self.client = client
        self.c = c
        self.conn = conn

    def sleep(self, setTime:float):
        time.sleep(setTime)

bot = Toolkit(client, c, conn)

c.execute("""CREATE TABLE IF NOT EXISTS threads (
    user_id integer,
    thread_id integer,
    guild_id integer,
    embedmsg_id integer
)""")

c.execute("""CREATE TABLE IF NOT EXISTS channels (
    channel_id integer,
    guild_id integer,
    str_val1 text,
    str_val2 text
)""")

# Values
# str_val1: warn_msg
# str_val2: default_thread_name

# Channel type
# 0 - Filter

c.execute("""CREATE TABLE IF NOT EXISTS feeds (
    name text,
    rss text,
    channel_id integer,
    last_id text
)""")

@tasks.loop(minutes=5, reconnect=True)
async def feed_check():
    for name, rss_url, last_id, channel_id in c.execute(
        "SELECT name, rss, last_id, channel_id FROM feeds"
    ).fetchall():
        feed = feedparser.parse(rss_url)

        if len(feed.entries) == 0:
            print("Feed returned no entries:\n", feed)
            continue

        channel = client.get_channel(channel_id)
        if not channel:
            channel = client.get_partial_messageable(channel_id)

        for entry in feed.entries:
            if entry.id == last_id:
                break

            await channel.send(f"A new story has been posted: **{entry.title}**\n{entry.link}")

        c.execute(f"UPDATE feeds SET last_id = ? WHERE name = ?", (feed.entries[0].id, name))
        conn.commit()

@client.event
async def on_ready():
    embeds_id = c.execute("SELECT embedmsg_id FROM threads").fetchall()
    for msg_id in embeds_id:
        try:
            thread = await client.fetch_channel(c.execute(f"SELECT thread_id FROM threads WHERE embedmsg_id = {msg_id[0]}").fetchone()[0])
            embedmsg = await thread.fetch_message(msg_id[0])
            await embedmsg.delete()
        except:
            pass 

    print(f"{client.user.name} is ready")
    feed_check.start()

@client.event
async def on_application_command_error(interaction:Interaction, error):
    error = getattr(error, "original", error)
    if isinstance(error, application_checks.ApplicationMissingPermissions):
        await interaction.response.send_message(f"{error}", ephemeral=True)
    if isinstance(error, nextcord.errors.Forbidden):
        await doLog(bot, f"⚠ Error: `{error}`")
        raise error
    if isinstance(error, application_checks.ApplicationNotOwner):
        await interaction.response.send_message(f"{error}", ephemeral=True)
    else:
        await doLog(bot, f"⚠ Error: `{error}`")
        raise error

urlRegex = r"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)"

@client.event
async def on_message(message):
    filters = c.execute("SELECT channel_id FROM channels").fetchall()
    for y in client.get_all_application_commands():
                    if y.qualified_name == "rename":
                        rename_slash = y
    for x in filters:
        if message.channel.id == x[0]:
            if message.attachments or re.search(urlRegex, message.content):
                thread = await message.create_thread(name=c.execute(f"SELECT str_val2 FROM channels WHERE channel_id = {x[0]}").fetchone()[0].replace("$1", f"{message.author.name}"))
                embed = nextcord.Embed(title="New Discussion Thread", description=f"Use 📝 button to rename it.\n\nThis will only be valid for the first 10 minutes. To rename the thread afterwards use {rename_slash.get_mention(guild=None)} instead.", color=0x3366cc)
                embed.set_footer(icon_url=message.author.avatar ,text=f"This thread has been initialized and can only be renamed by {message.author.name}")
                embedmsg = await thread.send(embed=embed, view=threadView(bot, thread, message.author.id), delete_after=600)
                await thread.leave()
                sql = "INSERT INTO threads (user_id, thread_id, guild_id, embedmsg_id) VALUES (?, ?, ?, ?)"
                val = (message.author.id, thread.id, message.guild.id, embedmsg.id)
                c.execute(sql,val)
                conn.commit()
            elif message.type == nextcord.MessageType.pins_add:
                pass
            elif message.author.id == client.user.id:
                pass
            elif message.author.guild_permissions.manage_channels:
                pass
            else:
                warnMsg = c.execute(f"SELECT str_val1 FROM channels WHERE channel_id = {x[0]}").fetchone()[0].replace("$1", f"{message.channel.mention}")
                try:
                    await message.author.send(warnMsg)
                except:
                    await message.channel.send(f"{message.author.mention}\n{warnMsg}", delete_after=30)
                await message.delete()

@client.event
async def on_thread_delete(thread):
    check = c.execute(f"SELECT thread_id FROM threads WHERE thread_id = {thread.id}").fetchone()[0]
    if check:
        c.execute(f"DELETE FROM threads WHERE thread_id = {check}")
        conn.commit

@client.slash_command(description="Renames a registered discussion thread you started.")
async def rename(interaction:Interaction):
    try:
        thread_id = c.execute(f"SELECT thread_id FROM threads WHERE thread_id = {interaction.channel.id}").fetchone()[0]
        user_id = c.execute(f"SELECT user_id FROM threads WHERE thread_id = {thread_id}").fetchone()[0]
    except:
        thread_id = None
        user_id = None
    if interaction.channel.id == thread_id and interaction.user.id == user_id:
        thread = await client.fetch_channel(thread_id)
        if thread.locked:
            await interaction.response.send_message("This thread is locked.", ephemeral=True)
        else:
            await interaction.response.send_modal(renameModal(bot, thread))
    else:
        await interaction.response.send_message("This channel is either not a thread, not a registered thread or you don't own this thread.", ephemeral=True)

@client.slash_command(description="Check bot status, latency and data.")
async def stats(interaction:Interaction):
    await getPage(interaction, bot, 1, 3)

@client.slash_command(description="Make the bot join thread.")
@application_checks.has_permissions(manage_channels=True)
async def join_thread(interaction:Interaction):
    try:
        await interaction.channel.join()
    except:
        pass

@client.slash_command(description="Add a feed to current channel.")
@application_checks.is_owner()
async def add_feed(interaction:Interaction, rss:str = nextcord.SlashOption(description="RSS's Url"), name:str = nextcord.SlashOption(description="Give the feed a name.")):
    check = c.execute(f"SELECT name FROM feeds WHERE name = ?", (name,)).fetchone()
    if not check:
        sql = "INSERT INTO feeds (name, rss, channel_id, last_id) VALUES (?, ?, ?, ?)"
        val = (name, rss, interaction.channel.id, None)
        c.execute(sql,val)
        conn.commit()
        await interaction.send(f"Feed added to {interaction.channel.mention}.")
    else:
        await interaction.response.send_message("Name Already taken.")

@client.slash_command(description="Remove feed.")
@application_checks.is_owner()
async def rm_feed(interaction:Interaction, name:str = nextcord.SlashOption(description="Name of the feed")):
    check = c.execute(f"SELECT name FROM feeds WHERE name = ?", (name,)).fetchone()
    if check:
        c.execute(f"DELETE FROM feeds WHERE name = ?", (name,))
        conn.commit
        await interaction.send("Feed deleted.")
    else:
        await interaction.response.send_message("Feed doesn't exist.")

@client.slash_command(description="View all feeds.")
async def list_feed(interaction:Interaction):
    await getFeeds(interaction, bot, 1)

@client.slash_command(description="Add filter channels.")
@application_checks.has_permissions(manage_channels=True)
async def add_filter(interaction:Interaction, channel:nextcord.TextChannel = nextcord.SlashOption(description="Target channel (Text channel only).")):
    check = c.execute(f"SELECT channel_id FROM channels WHERE channel_id = {channel.id}").fetchone()
    if not check:
        await interaction.response.send_modal(filterModal(bot, channel, False))
    else:
        await interaction.response.send_message(f"{channel.mention} is already used.")

@client.slash_command(description="Remove filter channels.")
@application_checks.has_permissions(manage_channels=True)
async def rm_filter(interaction:Interaction, channel:nextcord.TextChannel = nextcord.SlashOption(description="Target channel (Text channel only).")):
    check = c.execute(f"SELECT channel_id FROM channels WHERE channel_id = {channel.id}").fetchone()
    if check:
        c.execute(f"DELETE FROM channels WHERE channel_id = {channel.id}")
        conn.commit()
        await interaction.response.send_message(f"{channel.mention} has been removed.")
    else:
        await interaction.response.send_message(f"{channel.mention} isn't a valid channel.")

@client.slash_command(description="Configure filter channel.")
@application_checks.has_permissions(manage_channels=True)
async def conf_filter(interaction:Interaction, channel:nextcord.TextChannel = nextcord.SlashOption(description="Target channel (Text channel only).")):
    check = c.execute(f"SELECT channel_id FROM channels WHERE channel_id = {channel.id}").fetchone()
    if check:
        await interaction.response.send_modal(filterModal(bot, channel, True))
    else:
        await interaction.response.send_message(f"{channel.mention} isn't a valid channel.")

@client.slash_command(description="Unregister a thread from the database.")
async def unregister(interaction:Interaction, thread:nextcord.Thread = nextcord.SlashOption(description="Target thread.")):
    check = c.execute(f"SELECT thread_id FROM threads WHERE thread_id = {thread.id}").fetchone()[0]
    if check:
        if interaction.user.id == c.execute(f"SELECT user_id FROM threads WHERE thread_id = {check}").fetchone()[0]:
            c.execute(f"DELETE FROM threads WHERE thread_id = {check}")
            await interaction.response.send_message("Thread unregistered.", ephemeral=True)
        elif interaction.user.guild_permissions.manage_channels:
            c.execute(f"DELETE FROM threads WHERE thread_id = {check}")
            await interaction.response.send_message("Thread unregistered.", ephemeral=True)
        else:
            await interaction.response.send_message("You do not own this thread.", ephemeral=True)
    else:
        await interaction.response.send_message("This thread isn't registered.", ephemeral=True)

@client.slash_command(description="Make a user unable to read specific channel.")
@application_checks.has_permissions(manage_channels=True)
async def hfu(interaction:Interaction, user:nextcord.Member):
    if interaction.channel.permissions_for(user).read_message_history == False:
        await interaction.channel.set_permissions(user, read_message_history=True)
        await interaction.response.send_message(f"{user.mention} can now read this channel.", ephemeral=True)
    else:
        await interaction.channel.set_permissions(user, read_message_history=False)
        await interaction.response.send_message(f"{user.mention} can no longer read this channel.", ephemeral=True)

@client.slash_command(description="Set status flag on suggestion channel thread.")
@application_checks.has_permissions(manage_threads=True)
async def mark(interaction:Interaction, tag:int = nextcord.SlashOption(description="Tag to give the post.", choices={"Accepted":0, "Refused":1, "Open":2})):
    try:
        if interaction.channel.parent_id == 1093212505603580055:
            acceptedTag = nextcord.utils.get(interaction.channel.parent.available_tags, id=1093267853513334804)
            refusedTag = nextcord.utils.get(interaction.channel.parent.available_tags, id=1093267939819536514)
            if tag == 0:
                await interaction.response.send_message("Post has been accepted. The thread is now locked.")
                newTags = interaction.channel.applied_tags
                try:
                    newTags.remove(refusedTag)
                except:
                    pass
                try:
                    newTags.append(acceptedTag)
                except:
                    pass
                await interaction.channel.edit(archived=True, locked=True, applied_tags=newTags)
            elif tag == 1:
                await interaction.response.send_message("Post has been refused. The thread is now locked.")
                newTags = interaction.channel.applied_tags
                try:
                    newTags.remove(acceptedTag)
                except:
                    pass
                try:
                    newTags.append(refusedTag)
                except:
                    pass
                await interaction.channel.edit(archived=True, locked=True, applied_tags=newTags)
            elif tag == 2:
                await interaction.response.send_message("Post has been re-openned.")
                newTags = interaction.channel.applied_tags
                try:
                    newTags.remove(refusedTag)
                except:
                    pass
                try:
                    newTags.remove(acceptedTag)
                except:
                    pass
                await interaction.channel.edit(archived=False, locked=False, applied_tags=newTags)
        else:
            await interaction.response.send_message("Not a valid forum channel.", ephemeral=True)
    except:
        await interaction.response.send_message("Please execute the command in a forum post.", ephemeral=True)

@client.slash_command(description="DEV FORUM TAG INFO")
async def dev_tags(interaction:Interaction):
    try:
        if interaction.channel.parent.available_tags:
            tags = ""
            for tag in interaction.channel.parent.available_tags:
                try:
                    tags += f"{tag.name} | {tag.id} | {tag.moderated}\n"
                except:
                    pass
            await interaction.response.send_message(tags, ephemeral=True)
    except:
        await interaction.response.send_message("--Invalid--", ephemeral=True)

@client.slash_command(description="Waschen help.")
async def help(interaction:Interaction):
    await getHelp(interaction, bot, 1)

@client.slash_command(description="Get a list of staff members.")
async def staff(interaction:Interaction):
    await memberListing(interaction, 0)

@client.slash_command(description="Export current time database file.")
async def export_db(interaction:Interaction):
    with open("waschen.sqlite3", "rb") as f:
        await interaction.response.send_message(f"This is {client.user.mention}'s database data as of current time. Use software such as [sqlitebrowser](https://sqlitebrowser.org/) to open the file.", file=nextcord.File(f), ephemeral=True)

try:
    client.run(os.getenv('TOKEN'))
except:
    print("Failed to connect.")
